import cv2
import numpy as np
from openni import openni2
from openni import _openni2 as c_api
from mtcnn.mtcnn import MTCNN
import face_recognization

openni2.initialize("../lib/Redist2/") #The OpenNI2 Redist folder
dev = openni2.Device.open_any()
depth_stream = dev.create_color_stream()
depth_stream.start()
depth_stream.set_video_mode(c_api.OniVideoMode(pixelFormat = c_api.OniPixelFormat.ONI_PIXEL_FORMAT_RGB888, resolutionX = 640, resolutionY = 480, fps = 30))

# id=input('Nhập mã nhân viên: ')
# name=input('Nhập tên nhân viên: ')
# print("Bắt đầu chụp ảnh nhân viên, Vui lòng nhìn vào camera!")

fontface = cv2.FONT_HERSHEY_SIMPLEX
fontscale = 0.6
fontcolor = (0,0,255)
# sampleNum=0
detector = MTCNN()

while True:
	frame = depth_stream.read_frame()
	frame_data = frame.get_buffer_as_uint8()
	img = np.frombuffer(frame_data, dtype=np.uint8)
	img.shape = (480, 640, 3)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
	img = cv2.resize(img, (480, 320))

	# sampleNum += 1
	# if sampleNum < 9:
	# 	cv2.imwrite(f'Dataset2/{id}_{name}_{sampleNum}.png', img)

	faces = detector.detect_faces(img)

	for face in faces:
		x1, y1, width, height = face['box']
		x1, y1 = abs(x1), abs(y1)
		x2, y2 = x1 + width, y1 + height
		# draw bouding scope
		cv2.rectangle(img, (x1, y1), (x2, y2), (0, 255, 0), 2)
		# extract the face
		pixels = img[y1:y2, x1:x2]
		name, accurate = face_recognization.scan(pixels)
		cv2.putText(img, f"Name: {name}/{round(accurate, 2)}", (x1, y2 + 20), fontface, fontscale, fontcolor, 2)
		# cv2.putText(img, "Name: Unknown", (x1, y2 + 20), fontface, fontscale, fontcolor, 2)
		
	cv2.imshow("image", img)
	
	if (cv2.waitKey(20) & 0xFF == ord('q')):
		break

openni2.unload()
