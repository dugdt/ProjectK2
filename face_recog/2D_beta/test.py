import cv2
import numpy as np
from openni import openni2
from openni import _openni2 as c_api

openni2.initialize("../lib/Redist2/") #The OpenNI2 Redist folder
dev = openni2.Device.open_any()
depth_stream = dev.create_color_stream()
depth_stream.start()
depth_stream.set_video_mode(c_api.OniVideoMode(pixelFormat = c_api.OniPixelFormat.ONI_PIXEL_FORMAT_RGB888, resolutionX = 640, resolutionY = 480, fps = 30))


while True:
	frame = depth_stream.read_frame()
	frame_data = frame.get_buffer_as_uint8()
	img = np.frombuffer(frame_data, dtype=np.uint8)
	img.shape = (480, 640, 3)
	
	cv2.imshow("image", img)
	
	if (cv2.waitKey(20) & 0xFF == ord('q')):
		break

openni2.unload()