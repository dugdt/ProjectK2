# develop a classifier for the 5 Celebrity Faces Dataset

from numpy import load
from numpy import expand_dims
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Normalizer
from sklearn.svm import SVC

from face_embedding import get_embedding
from keras.models import load_model
from PIL import Image
from numpy import asarray

def scan(pixels):
    # resize pixels to the model size
    required_size=(160, 160)
    image = Image.fromarray(pixels)
    image = image.convert('RGB')
    image = image.resize(required_size)
    face_array = asarray(image)
    model = load_model('Model/facenet_keras.h5')
    embedding = get_embedding(model, face_array)

    # load face embeddings
    data = load('faces-embeddings.npz')
    trainX, trainy = data['arr_0'], data['arr_1']
    # normalize input vectors
    in_encoder = Normalizer(norm='l2')
    trainX = in_encoder.transform(trainX)
    
    # label encode targets
    out_encoder = LabelEncoder()
    out_encoder.fit(trainy)
    trainy = out_encoder.transform(trainy)
    
    # fit model
    model = SVC(kernel='linear', probability=True)
    model.fit(trainX, trainy)
    
    # prediction for the face
    samples = expand_dims(embedding, axis=0)
    yhat_class = model.predict(samples)
    yhat_prob = model.predict_proba(samples)
    # get name
    class_index = yhat_class[0]
    class_probability = yhat_prob[0,class_index] * 100
    predict_names = out_encoder.inverse_transform(yhat_class)

    # print('Predicted: %s (%.3f)' % (predict_names[0], class_probability))
    return (predict_names[0], class_probability)
    